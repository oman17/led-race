/*
 * sensor.hpp
 *
 *  Created on: 22 pa� 2019
 *      Author: michal
 */

#ifndef SENSOR_HPP_
#define SENSOR_HPP_

#include "main.hpp"

int read_sensor(uint8_t player);
int calibration_sensor(uint8_t player);



#endif /* SENSOR_HPP_ */
