/*
 * main.hpp
 *
 *  Created on: 22 pa� 2019
 *      Author: michal
 *      JST SM -connector to LED
 */

#ifndef MAIN_HPP_
#define MAIN_HPP_

#include <Arduino.h>
#include <Adafruit_NeoPixel.h>
#include "MemoryFree.h"

/*********************************************************
 ********************** Defines **************************
 *********************************************************/

/***********************	USR		**********************/
#define _NUMBER_OF_LOOP		2			//total laps race
#define _NUMBER_OF_PLAYER	3			//player number
#define _NUMBER_OF_OBSTAC	2			//player of obstacles
#define _INACTIVITY_TIME	30  		//time to start disco in s

#define _ACEL				0.2			//acceleration after push	defualt: 0.2
#define _kf					0.015 		//friction constant			defualt:0.015
#define _kg					0.0002 		//gravity constant			defualt:0.003

//#define _DEBUG_MODE
//#define _DEBUG_GRAV_MAP

#define _TELEMTRY_VIEW
//#define _BLUETHOOT
//#define _OLED


#if defined _DEBUG_MODE && defined _TELEMTRY_VIEW
#error("Do NOT use debug and telemetry viewer in the same time")
#endif

#if defined _DEBUG_MODE && defined _DEBUG_GRAV_MAP
#error("Do NOT use debug and _DEBUG_GRAV_MAP in the same time")
#endif

#if defined _TELEMTRY_VIEW && defined _DEBUG_GRAV_MAP
#error("Do NOT use _TELEMTRY_VIEW and _DEBUG_GRAV_MAP in the same time")
#endif

#ifdef _OLED
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#endif




/***********************	HARDW	**********************/
#define _MAXLED				300		// MAX LEDs actives on track
#define _PIN_LED			A0  	// R 500 ohms to DI pin for WS2812 and WS2813, for WS2813 BI pin of first LED to GND  ,  CAP 1000 uF to VCC 5v/GND,power supplie 5V 2A
#define _PIN_P1				7  		 // switch player 1 to PIN and GND
#define _PIN_P2				6   	// switch player 2 to PIN and GND
#define _PIN_P3				5   	// switch player 2 to PIN and GND
#define _PIN_AUDIO     		3   	// through CAP 2uf to speaker 8 ohms
#define _TRACK_DELAY		5
#define _NPIXELS			_MAXLED

#define _MAX_NUMBER_OF_OBSTAC	_NUMBER_OF_OBSTAC		//if > _NUMBER_OF_OBSTAC+1 crash (LOL?) , program freez ,too many obstacle can caouse problem via RAM limit

#define _COLOR_RED				255,0,0
#define _COLOR_GREEN			0,255,0
#define _COLOR_BLUE				0,0,255
#define _COLOR_YELLOW			255,255,0
#define _COLOR_WHITE			255,255,255
#define _COLOR_PURPLE			85,26,139
#define _COLOR_TURKUSOWY		118,238,198
#define _NO_COLOR				0,0,0




/*********************************************************
 ********************** Structures ***********************
 *********************************************************/
struct sColor_my{
  int R;
  int G;
  int B;
};

struct sMusic{
	uint16_t win_music[9];
	uint8_t Number_beep;
	uint8_t Pin;
};

struct sTimestamp{
	uint64_t Start;
	uint64_t Actual;
	uint64_t Best;
	uint64_t Last_push;
};

struct sCarParam{
	uint8_t number;
	uint8_t distance;
	uint8_t loop;
};

/*********************************************************
 ********************** Enums ****************************
 *********************************************************/
typedef enum{
	_OBSTACLES_RAMP,
	_OBSTACLES_LOOP,
}eObstacles;


/*********************************************************
 ********************** Class ****************************
 *********************************************************/
class cObstacle{
public:
//Constructor
	cObstacle( eObstacles User_type,uint16_t center, uint16_t start,uint16_t end,uint8_t height, uint8_t *gravity_map ,Adafruit_NeoPixel *track);
	cObstacle();
	cObstacle(uint8_t *gravity_map ,Adafruit_NeoPixel *track);
//variable

//function
	void show_pixel();
	void remove_pixel();
	void set(uint8_t center, uint8_t start,uint8_t end,uint8_t height);
	void set_color(sColor_my color);
	void remove();
	eObstacles get_type();
private:
//variable
	eObstacles Type;
	uint8_t center;
	uint8_t start;
	uint8_t end;
	uint8_t height;
	sColor_my color;
	uint8_t *gravity_map;
	Adafruit_NeoPixel *track;

//function

};

class cCar{
public:
//Constructor
	cCar(sColor_my Color, uint8_t *gravity_map, Adafruit_NeoPixel *track);
	cCar();
//variable



//function
	bool check_win();
	void show_winer();
	float get_dist();
	uint8_t get_loop();
protected:
//variable
	float speed;
	float dist;
	sColor_my color;
	uint8_t loop;
	uint8_t *gravity_map;
	Adafruit_NeoPixel *track;

//function
	void run_Car();
	void reset_Car();
	void draw_car();
};


class cCarRace : public cCar{
public:
//Constructor
	cCarRace(sColor_my User_color, uint8_t *gravity_map, uint8_t pin ,Adafruit_NeoPixel *track);
	cCarRace();
//variable
	uint64_t Last_push;


//function
	bool run(); //return if pushed
	void reset();
private:
//variable
	uint8_t pin;
	uint8_t flag_sw;

//function

};


class cCarGhost : public cCar{
public:
//Constructor
	cCarGhost(sColor_my User_color,uint16_t track_distance, uint8_t *gravity_map ,Adafruit_NeoPixel *track);
	cCarGhost();
//variable


//function
	void run(Adafruit_NeoPixel *track, uint64_t time); //return if pushed
	void reset();
	void set_time(float time);
private:
//variable
	uint32_t  time;
	uint64_t time_last;
	uint16_t track_distance;

//function

};

class cFuture{
private:
	cFuture();
	void burning1();
	void burning2();
	void track_rain_fx();
	void track_oil_fx();
	void track_snow_fx();
	void fuel_empty();
	void fill_fuel_fx();
	void in_track_boxs_fx();
	void pause_track_boxs_fx();
	void flag_boxs_stop();
	void flag_boxs_ready();
	void draw_safety_car();
	void telemetry_rx();
	void telemetry_tx();
};

/*class cDisco{
public:
//function
	void Disco();
	bool check_push();

protected:
//variable
	bool Block_return;

//function
	void colorWipe(uint32_t color, int wait);
	void theaterChase(uint32_t color, int wait);
	void rainbow(int wait);
	void theaterChaseRainbow(int wait);
};*/

class cRace{
public:
//Constructor
		cRace();

//variable
	uint8_t leader;


//function
	void run();
	void setup();
	sCarParam get_car_param(uint8_t number);
	sCarParam get_leader_param(); 				//leader and distance
	float get_time();
	void set_color_all(sColor_my Color);
#ifdef _TELEMTRY_VIEW
	void send_telemtry_USB();
#endif

	//disco	todo:move to other class
	void Disco();
	bool check_push();
private:
//variable
	Adafruit_NeoPixel track;

	uint8_t gravity_map[_MAXLED];
	uint8_t draworder;
	uint8_t number_of_obstacle;
	sMusic speaker;
	sTimestamp timestamp;
	cObstacle obstacles[_MAX_NUMBER_OF_OBSTAC];
	cCarRace car[_NUMBER_OF_PLAYER];
	cCarGhost car_ghost;


//function
	void winner_fx();	//winner music
	void start_race();

	void reset();
	void reset_pixel();
	bool check_best_time();
	void Sound_Lap();
	void set_obstacle();
	inline void reset_grav_map();

	inline uint16_t obst_center(uint16_t start);
	inline uint16_t obst_end(uint16_t center);
	inline uint16_t obst_height();
	inline uint8_t obst_type();
	void accept_input_flash();


	//disco
	//variable
		bool Block_return;

	//function
		void colorWipe(uint32_t color, int wait);
		void theaterChase(uint32_t color, int wait);
		void rainbow(int wait);
		void theaterChaseRainbow(int wait);

};

#ifdef _OLED
class cDisplay{
public:
//Constructor
	cDisplay();

//variable
	Adafruit_SSD1306 display;


//function
	void display_lcd_laps();
	void display_lcd_time();
	void display_lcd_leader();
	void display_lcd_progres();
	void run();

private:
	uint8_t Draw_progress;
	uint8_t Car_progress[_NUMBER_OF_PLAYER];
};
#endif

/*********************************************************
 ********************** FUNCTION ***********************
 *********************************************************/


#endif /* MAIN_HPP_ */
