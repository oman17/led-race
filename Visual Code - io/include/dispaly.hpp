/*
 * dispaly.hpp
 *
 *  Created on: 22 pa� 2019
 *      Author: michal
 *      source: ssd1306_128x64_spi.ino adafruit example
 */

#ifndef DISPALY_HPP_
#define DISPALY_HPP_

#include "main.hpp"

#ifdef _OLED
/*********************************************************
 ********************** Defines **************************
 *********************************************************/

#if (SSD1306_LCDHEIGHT != 64)
#error("Height incorrect, please fix Adafruit_SSD1306.h!");
#endif

// If using software SPI (the default case):
#define _OLED_MOSI   11
#define _OLED_CLK   13
#define _OLED_DC    12
#define _OLED_CS    9
#define _OLED_RESET 8

/* Uncomment this block to use hardware SPI
#define OLED_DC     6
#define OLED_CS     7
#define OLED_RESET  8
Adafruit_SSD1306 display(OLED_DC, OLED_RESET, OLED_CS);
*/

#define _RECTANGLE_SEPP			5
#define _RECTANGLE_HEIGHT		10
#define _RECTANGLE_WIDTH		(SSD1306_LCDWIDTH - _RECTANGLE_SEPP*2)

/*********************************************************
 ********************** FUNCTION ***********************
 *********************************************************/



void Scroll_text(void);
#endif

#endif /* DISPALY_HPP_ */
