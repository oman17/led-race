/*
 * carGhost.cpp
 *
 *  Created on: 9 lis 2019
 *      Author: michal
 */

#include "main.hpp"


cCarGhost::cCarGhost(sColor_my User_color,uint16_t track_distance, uint8_t *gravity_map ,Adafruit_NeoPixel *track) : cCar(User_color,gravity_map ,track)
{
	this->track_distance=track_distance;
	this->time=99999;
	this->time_last=0;

}

cCarGhost::cCarGhost() : cCar()
{
	this->time=0;
	this->time_last=0;
	this->track_distance=_NPIXELS*_NUMBER_OF_LOOP;
	this->track=0;
}

void cCarGhost::reset()
{
	this->reset_Car();
	this->time_last=0;
}

void cCarGhost::set_time(float time)
{
	this->time=time;
}


void cCarGhost::run(Adafruit_NeoPixel *track, uint64_t time)
{
	if(this->time_last==0)
	{
		this->time_last=time-1;
	}

	uint32_t delta=time-this->time_last;
	float veloc=0;
	 this->speed= (float)this->track_distance / (float)(this->time);
	this->dist +=  this->speed *delta;

	veloc=this->speed *delta;

	this->time_last=time;

    if (this->dist > _NPIXELS * this->loop)
    {
    	this->loop++;
    }

	 this->draw_car();
}

