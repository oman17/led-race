/*
 * obstacles.cpp
 *
 *  Created on: 22 pa� 2019
 *      Author: michal
 */

#include "obstacles.hpp"


cObstacle::cObstacle( eObstacles User_type,uint16_t center, uint16_t start,uint16_t end,uint8_t height, uint8_t *gravity_map ,Adafruit_NeoPixel *track)
{

	this->Type=User_type;
	this->center=center;
	this->start=start;
	this->end=end;
	this->height=height;
	this->gravity_map=gravity_map;
	this->track=track;
	this->color={_COLOR_WHITE};

	if(this->Type == _OBSTACLES_RAMP)
	{
		for(int i=0;i<(this->end-this->start);i++)
		{
			this->gravity_map[this->start+i]=127-i*((float)this->height/(this->end-this->start));
		}
		this->gravity_map[this->end]=127;
		for(int i=0;i<(this->center-this->end);i++)
		{
			this->gravity_map[this->end+i+1]=127+this->height-i*((float)this->height/(this->center - this->end));
		}
	}else{
		for(int i=0;i<(this->end-this->start);i++)
		{
			this->gravity_map[this->start+i]=127-i*((float)this->height/(this->end-this->start));
		}
		this->gravity_map[this->end]=255;
		for(int i=0;i<(this->center-this->end);i++)
		{
			this->gravity_map[this->end+i+1]=127+this->height-i*((float)this->height/(this->center - this->end));
		}
	}
}

cObstacle::cObstacle(uint8_t *gravity_map ,Adafruit_NeoPixel *track)
{
	this->Type= _OBSTACLES_LOOP;
	this->center=0;
	this->start=0;
	this->end=0;
	this->height=0;
	this->gravity_map=gravity_map;
	this->track=track;

}

cObstacle::cObstacle()
{
	this->Type= _OBSTACLES_LOOP;
	this->center=0;
	this->start=0;
	this->end=0;
	this->height=0;
	this->gravity_map=0;
	this->track=0;
}

void cObstacle::show_pixel()
{
	 for(uint32_t i = this->start ; i < this->end ;i++)
	 {
		 this->track->setPixelColor(i,
				 (this->color.R-gravity_map[i])/8, (this->color.G-gravity_map[i])/8, (this->color.B-gravity_map[i])/8);
	 }
}
void cObstacle::remove_pixel()
{
	 for(int i=this->start;i<=this->end;i++)
	 {
		 this->track->setPixelColor(i, track->Color(_NO_COLOR));
	 }

}
void cObstacle::set(uint8_t center, uint8_t start,uint8_t end,uint8_t height)
{
	this->center=center;
	this->start=start;
	this->end=end;
	this->height=height;
}
void cObstacle::set_color(sColor_my color)
{
	this->color=color;
}

eObstacles cObstacle::get_type()
{
	return this->Type;
}
