/*
 * carRace.cpp
 *
 *  Created on: 9 lis 2019
 *      Author: michal
 */

#include "main.hpp"


cCarRace::cCarRace(sColor_my User_color, uint8_t *gravity_map, uint8_t pin ,Adafruit_NeoPixel *track) : cCar(User_color,gravity_map,track)
{
	this->pin=pin;

	this->Last_push=0;
	this->flag_sw=0;
	this->Last_push=0;

}

cCarRace::cCarRace() : cCar()
{
	this->pin=0;

	this->flag_sw=0;
	this->Last_push=0;
}

void cCarRace::reset()
{
	this->reset_Car();
	this->flag_sw=0;
}




bool cCarRace::run()
{
	bool return_v=0;
	//button
    if ( (this->flag_sw==1) && (digitalRead(this->pin)==0) )
    {
    	this->flag_sw=0;
    	this->speed+=_ACEL;
    	this->Last_push=millis();
    	return_v=1;
    };
    if ( (this->flag_sw==0) && (digitalRead(this->pin)==1) ) {this->flag_sw=1;};

   this->run_Car();

	return return_v;
}

