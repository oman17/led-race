/*
 * light_efect.cpp
 *
 *  Created on: 22 pa� 2019
 *      Author: michal
 */

#include "light_efect.hpp"

void cRace:: colorWipe(uint32_t color, int wait) {
  for(int i=0; i<track.numPixels(); i++) { // For each pixel in track...
    track.setPixelColor(i, color);         //  Set pixel's color (in RAM)
    track.show();                          //  Update track to match
    delay(wait);                           //  Pause for a moment
    	if( this->check_push()==true) 	return;
  }
}

// Theater-marquee-style chasing lights. Pass in a color (32-bit value,
// a la track.Color(r,g,b) as mentioned above), and a delay time (in ms)
// between frames.
void cRace:: theaterChase(uint32_t color, int wait) {
  for(int a=0; a<10; a++) {  // Repeat 10 times...
    for(int b=0; b<3; b++) { //  'b' counts from 0 to 2...
      track.clear();         //   Set all pixels in RAM to 0 (off)
      // 'c' counts up from 'b' to end of track in steps of 3...
      for(int c=b; c<track.numPixels(); c += 3) {
        track.setPixelColor(c, color); // Set pixel 'c' to value 'color'
        if( this->check_push()==true) 	return;
      }
      track.show(); // Update track with new contents
      delay(wait);  // Pause for a moment
    }
  }
}

// Rainbow cycle along whole track. Pass delay time (in ms) between frames.
void cRace:: rainbow(int wait) {
  // Hue of first pixel runs 5 complete loops through the color wheel.
  // Color wheel has a range of 65536 but it's OK if we roll over, so
  // just count from 0 to 5*65536. Adding 256 to firstPixelHue each time
  // means we'll make 5*65536/256 = 1280 passes through this outer loop:
  for(long firstPixelHue = 0; firstPixelHue < 5*65536; firstPixelHue += 256) {
    for(int i=0; i<track.numPixels(); i++) { // For each pixel in track...
      // Offset pixel hue by an amount to make one full revolution of the
      // color wheel (range of 65536) along the length of the track
      // (track.numPixels() steps):
      int pixelHue = firstPixelHue + (i * 65536L / track.numPixels());
      // track.ColorHSV() can take 1 or 3 arguments: a hue (0 to 65535) or
      // optionally add saturation and value (brightness) (each 0 to 255).
      // Here we're using just the single-argument hue variant. The result
      // is passed through track.gamma32() to provide 'truer' colors
      // before assigning to each pixel:
      track.setPixelColor(i, track.gamma32(track.ColorHSV(pixelHue)));
      if( this->check_push()==true) 	return;
    }
    track.show(); // Update track with new contents
    delay(wait);  // Pause for a moment
  }
}

// Rainbow-enhanced theater marquee. Pass delay time (in ms) between frames.
void cRace:: theaterChaseRainbow(int wait) {
  int firstPixelHue = 0;     // First pixel starts at red (hue 0)
  for(int a=0; a<30; a++) {  // Repeat 30 times...
    for(int b=0; b<3; b++) { //  'b' counts from 0 to 2...
      track.clear();         //   Set all pixels in RAM to 0 (off)
      // 'c' counts up from 'b' to end of track in increments of 3...
      for(int c=b; c<track.numPixels(); c += 3) {
        // hue of pixel 'c' is offset by an amount to make one full
        // revolution of the color wheel (range 65536) along the length
        // of the track (track.numPixels() steps):
        int      hue   = firstPixelHue + c * 65536L / track.numPixels();
        uint32_t color = track.gamma32(track.ColorHSV(hue)); // hue -> RGB
        track.setPixelColor(c, color); // Set pixel 'c' to value 'color'
      }
      track.show();                // Update track with new contents
      delay(wait);                 // Pause for a moment
      firstPixelHue += 65536 / 90; // One cycle of color wheel over 90 frames
       if( this->check_push()==true) 	return;
    }
  }
}


void cRace::Disco()
{    //neopixel demo
  // Fill along the length of the track in various colors...

	this->reset_pixel();
	this->Block_return=0;

  this->colorWipe(this->track.Color(255,   0,   0), 10); // Red
  if( this->check_push()==true) 	{this->start_race();return;}
  this->colorWipe(this->track.Color(  0, 255,   0), 10); // Green
   if( this->check_push()==true) 	{this->start_race();return;}
  this->colorWipe(this->track.Color(  0,   0, 255), 10); // Blue
   if( this->check_push()==true) 	{this->start_race();return;}

  // Do a theater marquee effect in various colors...
  this->theaterChase(this->track.Color(127, 127, 127), 50); // White, half brightness
   if( this->check_push()==true) 	{this->start_race();return;}
  this-> theaterChase(this->track.Color(127,   0,   0), 50); // Red, half brightness
   if( this->check_push()==true) 	{this->start_race();return;}
  this-> theaterChase(this->track.Color(  0,   0, 127), 50); // Blue, half brightness

  this->rainbow(10);             // Flowing rainbow cycle along the whole track
   if( this->check_push()==true) 	{this->start_race();return;}
  this->theaterChaseRainbow(50); // Rainbow-enhanced theaterChase variant
   if( this->check_push()==true) 	{this->start_race();return;}
}

bool cRace::check_push()
{
	extern uint8_t  Pin_player[_NUMBER_OF_PLAYER];
	if( this->Block_return==1)
	{
		return true;
	}else
	{
		for(uint8_t i=0;i<_NUMBER_OF_PLAYER;i++)
		{
			if(digitalRead(Pin_player[i])==0)
			{
				this->timestamp.Last_push=millis();
				this->Block_return=1;
				return true;
			}
		}
	}
	return false;

}

