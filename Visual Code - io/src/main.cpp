/*
 * ____                     _      ______ _____    _____
  / __ \                   | |    |  ____|  __ \  |  __ \
 | |  | |_ __   ___ _ __   | |    | |__  | |  | | | |__) |__ _  ___ ___
 | |  | | '_ \ / _ \ '_ \  | |    |  __| | |  | | |  _  // _` |/ __/ _ \
 | |__| | |_) |  __/ | | | | |____| |____| |__| | | | \ \ (_| | (_|  __/
  \____/| .__/ \___|_| |_| |______|______|_____/  |_|  \_\__,_|\___\___|
        | |
        |_|
 Open LED Race
 An minimalist cars race for LED track

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 by gbarbarov@singulardevices.com  for Arduino day Seville 2019

 Code made dirty and fast, next improvements in:
 https://github.com/gbarbarov/led-race
 https://www.hackster.io/gbarbarov/open-led-race-a0331a
 https://twitter.com/openledrace
*/

/**
 *
	******************************************************************************
	* @file         : main.cpp
	* @brief        : Main file of project.
	* @date			: 22.10.2019
	* @author       : EDIT BY: Micha� Zalas, Kamil Koto�ski , GPLv3 License , https://gitlab.com/oman17/led-race
	******************************************************************************
*
 **/
//doxygen standard

#include "main.hpp"
#include "light_efect.hpp"
#include "obstacles.hpp"
#include "race.hpp"
#include "sensor.hpp"


cRace Race;

struct sColor_my Color_player[_NUMBER_OF_PLAYER]=
{
		{_COLOR_BLUE},{_COLOR_YELLOW},{_COLOR_GREEN}
};


uint8_t  Pin_player[_NUMBER_OF_PLAYER]=
{
		_PIN_P1,_PIN_P2,_PIN_P3
};

/**
  * @brief  The application entry point.
  * @retval None
  */
void setup() {
#ifdef _TELEMTRY_VIEW
  Serial.begin(57600);
#endif
  Race.setup();

}

/**
  * @brief  The application main run.
  * @retval None
  */
void loop() {
	Race.run();
}
