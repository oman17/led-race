/*
 * car.cpp
 *
 *  Created on: 29 pa� 2019
 *      Author: michal
 */


#include "car.hpp"

cCar::cCar(sColor_my Color, uint8_t *gravity_map, Adafruit_NeoPixel *track)
{
	this-> color=Color;
	this->gravity_map=gravity_map;
	this->track=track;

	this->speed=0;
	this->dist=0;
	this->loop=0;

}

cCar::cCar()
{
	this-> color={_NO_COLOR};
	this->gravity_map=0;
	this->track=0;

	this->speed=0;
	this->dist=0;
	this->loop=0;

}

void cCar::reset_Car()
{
	this->speed=0;
	this->dist=0;
	this->loop=0;

}

bool cCar::check_win()
{
	if( this->loop>_NUMBER_OF_LOOP)
	{
		return true;
	}else{
		return false;
	}
}

void cCar::show_winer()
{
	extern cRace Race;	//todo: think about better solution
	Race.set_color_all(this->color);
	this->track->show();
}

void cCar::run_Car()
{
	//accelerete
    if ((this->gravity_map[(word)this->dist % _NPIXELS])<127)	//up
	{
		this->speed -=_kg*(127-(gravity_map[(word)this->dist % _NPIXELS]));
	}else if ((this->gravity_map[(word)this->dist % _NPIXELS])>127)	//down
	{
		this->speed += _kg*((gravity_map[(word)this->dist % _NPIXELS])-127);
	}


    this->speed -=  this->speed * _kf;
    this->dist +=  this->speed;

    if (this->dist > _NPIXELS * this->loop)
    {
    	this->loop++;
    }

	//draw
    this->draw_car();

}

float cCar::get_dist()
{
	return this->dist;
}

uint8_t cCar::get_loop()
{
	return this->loop;
}

void cCar::draw_car()
{
	sColor_my color_temp;
		for(int i=0;i<=this->loop;i++)
		{
			if(this->color.R > 20)
			{
				color_temp.R= this->color.R-i*20;
			}else{
				color_temp.R= this->color.R;
			}
			if(this->color.G > 20)
			{
				color_temp.G= this->color.G-i*20;
			}else{
				color_temp.G= this->color.G;
			}
			if(this->color.B > 20)
			{
				color_temp.B= this->color.B-i*20;
			}else{
				color_temp.B= this->color.B;
			}

			uint32_t colorp =this->track->gamma32(track->Color(color_temp.R,color_temp.G,color_temp.B));
			this->track->setPixelColor(((word)this->dist % _NPIXELS)+i, colorp);
		};
}
