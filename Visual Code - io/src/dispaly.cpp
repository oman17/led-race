/*
 * Dispaly.cpp
 *
 *  Created on: 22 pa� 2019
 *      Author: michal
 */


#include "dispaly.hpp"

#ifdef _OLED

extern cRace Race;

cDisplay::cDisplay()
{
	this->display = Adafruit_SSD1306 (_OLED_MOSI, _OLED_CLK, _OLED_DC, _OLED_RESET, _OLED_CS);
	this->display.begin(SSD1306_SWITCHCAPVCC);

	this->display.setTextSize(1); //1 is default 6x8, 2 is 12x16, 3 is 18x24, etc
	this->display.setTextColor(WHITE);
	this->display.setCursor(0,0);
	this->Draw_progress=0;
}

void cDisplay::display_lcd_laps(){

}

void cDisplay::display_lcd_time(){
  //to do

	this->display.setCursor(0,0); //set crusor to position

	//this->display.write();
	display.println();
	 display.display();
}

void cDisplay::display_lcd_leader(){
  //to do
	this->Draw_progress=0;
}

void cDisplay::display_lcd_progres(){
	if(this->Draw_progress==0)
	{
		for(uint8_t i=0; i< sizeof(Car_progress);i ++)
		{
			this->display.drawRect(_RECTANGLE_SEPP ,_RECTANGLE_HEIGHT*(i+1) + _RECTANGLE_SEPP*(i) ,_RECTANGLE_WIDTH ,_RECTANGLE_HEIGHT ,WHITE);
		}
	}
	this->Draw_progress=1;

	for(uint8_t i=0; i< sizeof(Car_progress);i ++)
	{
		sCarParam Car;
		Car = Race.get_car_param(i);
		this->Car_progress[i]= Car.distance/( _NUMBER_OF_LOOP * _NPIXELS );
	}

	for(uint8_t i=0; i< sizeof(Car_progress);i ++)
	{
		this->display.fillRect(_RECTANGLE_SEPP ,_RECTANGLE_HEIGHT , (uint8_t)(this->Car_progress[i] * _RECTANGLE_WIDTH) ,_RECTANGLE_HEIGHT,WHITE);
	}
	this->display.display();

}

void cDisplay::run(){
	this->display_lcd_progres();

}


#endif
