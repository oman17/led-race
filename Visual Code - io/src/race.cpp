/**
 *
	******************************************************************************
	* @file         : race.cpp
	* @brief        : This file provides code for race class.
	* @date			:22.10.2019
	* @author		:Michal Zalas
	******************************************************************************
*
 **/

#include "race.hpp"

extern struct sColor_my Color_player[_NUMBER_OF_PLAYER];
extern uint8_t  Pin_player[_NUMBER_OF_PLAYER];

#ifdef _OLED
cDisplay Display_my;
#endif


/**
  * @brief  This is setup f02,
  * or race game
  * @param  None
  * @retval None
  */

void cRace::setup(){
	_NUMBER_OF_OBSTAC;
	this->obstacles[0]=cObstacle(_OBSTACLES_RAMP ,200 ,170 ,230 ,30 , this->gravity_map , &this->track);
	this->obstacles[1]=cObstacle(_OBSTACLES_LOOP ,50 ,20 ,70 	,10 , this->gravity_map , &this->track);

	for(uint8_t i=0;i<sizeof(Pin_player);i++)
	{
		pinMode(Pin_player[i],INPUT_PULLUP);
	}

	this->track.begin();
	this->track.setBrightness(255); //max 255

#ifdef _DEBUG_MODE
	Serial.println("Push buttom to set obstacle.");
#endif

	this->set_color_all({_COLOR_WHITE});
	this->track.show();
	delay(3000);

	//if push 2 button set new obstacle if not use default
	if(digitalRead(Pin_player[0])==0 && digitalRead(Pin_player[1])==0 )
	{
		this->set_color_all({_COLOR_TURKUSOWY});
		this->track.show();
		delay(1000);
		if(digitalRead(Pin_player[0])==0 && digitalRead(Pin_player[1])==0 )
		{
			this->set_obstacle();
		}
	}

#ifdef _DEBUG_MODE
	Serial.println("End setup");
#endif

#ifdef _DEBUG_GRAV_MAP
	delay(10000);
	Serial.println("Gravity map");

	for(uint16_t i=0; i < sizeof(this->gravity_map);i++)
	{
		Serial.println(String(this->gravity_map[i])+ ";");
		delay(10);
	}
#endif

#ifdef _TELEMTRY_VIEW
	//this->send_telemtry_USB(); //todo: crash race ,  faster refresh of telemtry view
#endif
	this->start_race();

}

void cRace::run()
{
	this->timestamp.Actual=millis();
    if ((millis() & 512)==(512 * this->draworder))		//change order of car display
    {

    	if (this->draworder==0)
    	{
    		digitalWrite(LED_BUILTIN, HIGH);
    		this->draworder=1;
    	}else {
    		this->draworder=0;
    		digitalWrite(LED_BUILTIN, LOW);
    	}
    }

    this->reset_pixel();

	for(uint8_t i=0; i < this->number_of_obstacle; i++)
	{
		this->obstacles[i].show_pixel();
	}



    if (draworder==0)
    {

    	for(uint8_t i=0; i<_NUMBER_OF_PLAYER; i++)
    	{
    		if(car[i].run()==true)
    		{
    			this->timestamp.Last_push=car[i].Last_push;
    		}
    	}
    } else {

    	for(uint8_t i=_NUMBER_OF_PLAYER; i>0; i--)
    	{
    		if(car[i-1].run()==true)
    		{
    			this->timestamp.Last_push=car[i-1].Last_push;
    		}
    	}
    }

    car_ghost.run(&this->track, this->timestamp.Actual);


	float max_dist=0;
	for(uint8_t i=0; i<_NUMBER_OF_PLAYER; i++)
	{
		if(car[i].get_dist()>max_dist)
		{
			this->leader=i;
			max_dist=car[i].get_dist();
		}

		if(car[i].check_win()==true)
		{
			this->check_best_time();
#ifdef _TELEMTRY_VIEW
			//this->send_telemtry_USB(); //todo: race crash
#endif

			car[i].show_winer();
			track.show();
			this->car_ghost.set_time(this->timestamp.Best);
			this->car_ghost.reset();
		    this->winner_fx();
		    this->start_race();
			break;

		}
	}


	if(car_ghost.check_win()==true)
	{
		this->car_ghost.reset();
	}


	this->Sound_Lap();

    this->track.show();
    delay(_TRACK_DELAY);


    if (this->speaker.Number_beep>0)
    {
    	this->speaker.Number_beep-=1;
		if (this->speaker.Number_beep==0) // lib conflict !!!! interruption off by neopixel
		{
			noTone(this->speaker.Pin);
		}
	}

#ifdef _OLED
    Display_my.run();
#endif


#ifdef _TELEMTRY_VIEW
   this->send_telemtry_USB();
#endif

    //DISCO !!!!!!!!!!!! :D
    if(this->timestamp.Last_push + _INACTIVITY_TIME*1000<millis())
    {
      this->Disco();
    }

}



cRace::cRace(){
		memset(this->gravity_map,127,sizeof(gravity_map));
		this->leader=0;
		this->track= Adafruit_NeoPixel(_MAXLED, _PIN_LED, NEO_GRB + NEO_KHZ800);
		this->draworder=0;
		this->number_of_obstacle=_NUMBER_OF_OBSTAC;
		this->timestamp.Start=0;
		this->timestamp.Actual=0;
		this->timestamp.Last_push=0;
		this->timestamp.Best=999999;

		this->speaker.Pin= _PIN_AUDIO;
		this->speaker.Number_beep=3;
		uint16_t music[sizeof(this->speaker.win_music)/sizeof(uint16_t)]={
				2637, 2637, 0, 2637,
				0, 2093, 2637, 0,
				3136
		};
		memcpy(this->speaker.win_music,music,sizeof(this->speaker.win_music));


		for(uint8_t i=0; i<_NUMBER_OF_PLAYER; i++)
		{
			this->car[i]=cCarRace(Color_player[i], &this->gravity_map[0], Pin_player[i] ,&this->track);
		}

		for(uint8_t i=0; i<_MAX_NUMBER_OF_OBSTAC; i++)
		{
			this->obstacles[i]=cObstacle(this->gravity_map ,&this->track);
		}

		this->car_ghost =cCarGhost( {_COLOR_RED} , _NPIXELS*_NUMBER_OF_LOOP , &this->gravity_map[0] ,&this->track);
		this->car_ghost.set_time(this->timestamp.Best);

		this->Block_return=0;
}



sCarParam cRace::get_leader_param()
{
	struct sCarParam leader;
	leader.number = this->leader;
	leader.distance = this->car[this->leader].get_dist();
	leader.loop = this->car[this->leader].get_loop();
	return leader;
}

sCarParam cRace::get_car_param(uint8_t number)
{
	struct sCarParam car;
	car.number=this->leader;
	car.distance=this->car[number].get_dist();
	car.distance=this->car[number].get_loop();
	return car;
}

float cRace::get_time()
{
	return this->timestamp.Actual;
}

void cRace::set_color_all(sColor_my Color)
{
	this->track.clear();


	if((Color.R>200 && Color.G>200)
			|| (Color.R>200 && Color.B>200)
			|| (Color.B>200 && Color.G>200 ))		// when set to bright color, it consume too many current and turn red/rainbow color
	{
		Color.R *=0.3;
		Color.G *=0.3;
		Color.B *=0.3;
	}else if((Color.R>130 && Color.G>130)
			|| (Color.R>130 && Color.B>130)
			|| (Color.B>130 && Color.G>130) )
	{
		Color.R *=0.6;
		Color.G *=0.6;
		Color.B *=0.6;
	}

	uint32_t colorp =this->track.gamma32(this->track.Color(Color.R,Color.G,Color.B));

	this->track.fill(colorp,1);

}


#ifdef _TELEMTRY_VIEW
/**
  * @brief  send telemtry throught usb connection
  * @source None
  * @param  None
  * @retval None
  */
void cRace::send_telemtry_USB()
{
    char Telementry[20];
    int freeRam=freeMemory();

    sprintf(Telementry, "%d,%d,%d,%d,%d,%u",
    		freeRam, (uint16_t)this->car[0].get_dist(), (uint16_t)this->car[1].get_dist(),(uint16_t)this->car[2].get_dist(),
			this->leader,this->timestamp.Best);
    Serial.println(Telementry);
}
#endif


/**
  * @brief  move pixel all around track and when push button stop ask for param
  * @source https://www.norwegiancreations.com/2017/12/arduino-tutorial-serial-inputs/
  * @param  None
  * @retval None
  */
void cRace::set_obstacle()
{
	this->reset_pixel();
	this->reset_grav_map();
	this->number_of_obstacle=0;

	this->set_color_all({_NO_COLOR});
	this->track.show();
	delay(1000);

	while(this->number_of_obstacle < _MAX_NUMBER_OF_OBSTAC)
	{
		for(uint16_t pixel=0; pixel < _NPIXELS -20 ; pixel++)
		{
				this->track.setPixelColor(pixel, this->track.Color(_COLOR_WHITE));
				this->track.show();
				delay(100);
				if(digitalRead(Pin_player[0])==0)
				{
#ifdef _DEBUG_MODE
					Serial.print("Pixel: " + pixel); //todo: to many serial crash RAM
					Serial.println(" Wybierz srodek");
#endif

					delay(1000);
					uint16_t center=this->obst_center(pixel);
					this->track.setPixelColor(center, this->track.Color(_COLOR_WHITE));
					delay(1000);

#ifdef _DEBUG_MODE
					Serial.println("Wybierz koniec.");
#endif
					uint16_t end=this->obst_end(center);
					this->track.setPixelColor(end, this->track.Color(_COLOR_WHITE));
					delay(1000);

#ifdef _DEBUG_MODE
					Serial.println("Wybierz wysokosc.");
#endif
					this->reset_pixel();
					uint8_t height=this->obst_height();
					delay(1000);

#ifdef _DEBUG_MODE
					Serial.println("Wybierz typ, zielony-ramp,czerowony-loop.");
#endif
					uint8_t typ=this->obst_type();
					delay(1000);

					eObstacles type2=static_cast<eObstacles>(typ);
					this->obstacles[number_of_obstacle]= cObstacle(type2 ,center ,pixel ,end ,height , this->gravity_map ,&this->track);
					this->number_of_obstacle++;

					this->accept_input_flash();

#ifdef _DEBUG_MODE
					Serial.println("Aby przerwac wcisnij i przytrzymaj przycisk 1");
#endif
					delay(5000);
					if(digitalRead(Pin_player[0])==0)		//next obst ?
					{
						return;								//no
					}
					this->reset_pixel();					//yes

				} //set obstacle

				this->track.setPixelColor(pixel, this->track.Color(_NO_COLOR));
		}
	}



}

inline uint16_t cRace::obst_center(uint16_t start)
{
	uint16_t center=start;
	while(digitalRead(Pin_player[0])==1)
	{
		center++;
		if(center> _NPIXELS-20)
		{
			center=start+1;
		}

		this->track.setPixelColor(center, this->track.Color(_COLOR_WHITE));
		this->track.show();

		uint16_t delay_my=150;
		while(digitalRead(Pin_player[0])==1 && delay_my>1)
		{
			delay_my--;
			delay(1);
		}

		this->track.setPixelColor(center, this->track.Color(_NO_COLOR));
	}
	return center;
}


//! set obstacle end if player push buttom
/*!
 *
 *
*/
inline uint16_t cRace::obst_end(uint16_t center)
{
	uint16_t end=center;
	while(digitalRead(Pin_player[0])==1)
	{
		end++;
		if(end> _NPIXELS-10)
		{
			end=center+1;
		}

		this->track.setPixelColor(end, this->track.Color(_COLOR_WHITE));
		this->track.show();

		uint16_t delay_my=150;
		while(digitalRead(Pin_player[0])==1 && delay_my>1)
		{
			delay_my--;
			delay(1);
		}

		this->track.setPixelColor(end, this->track.Color(_NO_COLOR));
	}
	return end;
}


inline uint16_t cRace::obst_height()
{
	uint8_t height=0;
	uint32_t colorp =this->track.gamma32(this->track.Color(_COLOR_WHITE));
	this->track.setPixelColor(255, colorp);

	this->track.setBrightness(100);			//cosume to much amper
	while(digitalRead(Pin_player[0])==1)
	{
		static uint8_t draw=0;
		if(height> 254)
		{
			draw=0;
		}else if(height<= 1)
		{
			draw=1;
		}

		if(draw)
		{
			height++;
			this->track.setPixelColor(height, colorp);

		}else{
			this->track.setPixelColor(height, this->track.Color(_NO_COLOR));
			height--;

		}
		this->track.show();

		uint16_t delay_my=80;
		while(digitalRead(Pin_player[0])==1 && delay_my>1)
		{
			delay_my--;
			delay(1);
		}
	}
	this->track.setBrightness(255);
	return height;
}


inline uint8_t cRace::obst_type()
{
	uint8_t typ=0;
	while(digitalRead(Pin_player[0])==1)
	{
		typ++;
		if(typ>1)
		{
			typ=0;
		}

		if(typ==0)
		{
			this->set_color_all({_COLOR_GREEN});
		}else{
			this->set_color_all({_COLOR_RED});
		}

		this->track.show();

		uint16_t delay_my=1000;
		while(digitalRead(Pin_player[0])==1 && delay_my>1)
		{
			delay_my--;
			delay(1);
		}
	}

	return typ;
}

void cRace::accept_input_flash()
{
	this->set_color_all({_COLOR_GREEN});
	this->track.show();
	delay(1000);

	this->set_color_all({_NO_COLOR});
	this->track.show();
	delay(1000);

	this->set_color_all({_COLOR_GREEN});
	this->track.show();
	delay(1000);

	this->set_color_all({_NO_COLOR});
	this->track.show();
}


void cRace::reset()
{
	this->reset_pixel();
	for(uint8_t i=0; i< _NUMBER_OF_PLAYER;i++)
	{
		car[i].reset();
	}
}

void cRace::reset_pixel()
{
	this->track.clear();
}

void cRace::start_race()
{

	this->reset();

	for(uint8_t i=0; i<this->number_of_obstacle; i++) //show obstacles
	{
		this->obstacles[i].show_pixel();
	}

	this->track.show();
	delay(2000);
	this->track.fill(this->track.Color(_COLOR_RED),15,10);
	this->track.show();
	tone(this->speaker.Pin,400);

	delay(2000);
	noTone(this->speaker.Pin);
	this->track.fill(this->track.Color(_NO_COLOR),15,10);
	this->track.fill(this->track.Color(_COLOR_YELLOW),5,10);
	this->track.show();
	tone(this->speaker.Pin,600);

	delay(2000);
	noTone(this->speaker.Pin);
	this->track.fill(this->track.Color(_NO_COLOR),5,10);
	this->track.fill(this->track.Color(_COLOR_GREEN),1,10);
	this->track.show();
	tone(this->speaker.Pin,1200);
	delay(2000);

	noTone(this->speaker.Pin);
	this->timestamp.Start=millis();
	this->timestamp.Actual=millis();
}

//winner music
void cRace::winner_fx()
{
	   int msize = sizeof(this->speaker.win_music) / sizeof(int);
	   for (int note = 0; note < msize; note++)
	   {
		   tone(this->speaker.Pin, this->speaker.win_music[note],200);
		   delay(230);
		   noTone(this->speaker.Pin);
	   }
}

void cRace::Sound_Lap()
{
	for(uint8_t i;i<_NUMBER_OF_PLAYER;i++)
	{
		if (this->car[i].get_dist() > _NPIXELS * this->car[i].get_loop())
		{
			tone(speaker.Pin,600);
			speaker.Number_beep=2;
		}
	}

}

bool cRace::check_best_time()
{

	unsigned long timestamp_end=millis();
    uint32_t czas_przejazdu=timestamp_end - this->timestamp.Start;

#ifdef _DEBUG_MODE
    Serial.println("Gracz 3(zielony). Najlepszy wynik: " + String((uint32_t)this->timestamp.Best) +"[ms] Czas osiagniety: " + String((uint32_t)czas_przejazdu) +"[ms] \n");
#endif

    if(czas_przejazdu < this->timestamp.Best)
    {

#ifdef _DEBUG_MODE
      Serial.println("Pobiles rekord, twoj czas: "+ String((uint32_t)czas_przejazdu) +"[ms] \n");
#endif

      this->timestamp.Best=czas_przejazdu;
      return true;
    }
    return false;
}

inline void cRace::reset_grav_map()
{
	memset(this->gravity_map,127,sizeof(gravity_map));
}





