# led-race
Minimalist car race with a strip of LEDs.
Connect two switches between common GND and pins 6 and 7 and one led strip ws2812 or ws2813 to  GND, + 5V and A0 to a arduino Nano. After sometime of not use (idle time) it will dispaly light efect on track. It have AI player (red one) which is the best player who play until power on, on this AI player the gravity don't work. Projcet have telemtry eg.progress,time, leader through  USB cable (COM). Branch Arduino_IDE have basics .ino project it is EOL(end of life), it have some fixes in implmentation compared to the original.   
Link to git repository: https://gitlab.com/oman17/led-race.   
Build instruction: https://create.arduino.cc/projecthub/gbarbarov/open-led-race-a0331a  
Link to original repostory: https://github.com/gbarbarov/led-race



___
#### HOW TO RUN:

0. Install Visual studio code or upload .hex file (Release folder) to Arduino Nano and go to point 6.
1. Install PlatformIO
4. Connect PC to arduino via USB cable. 
5. Upload project : PlatformIO -> Upload 
6. Run Telemetry-> TelemetryViewer_v0.6.jar
7. Open Layout Telemetry_default.txt , set 5700 boudrate and COM port , and connect . Window will pop up, clik "Done". Now it will display basic information about race
8. If you only see one plot wait until game start , plot should apprer when game start (java bug)

___
#### HOW TO PLAY: 
-  When program start it turns White if you press player 1 and player 2 buttom and hold it until light turn off you will set the obstacles. Now you control setting obstacles by player one buttom. You set : beggining, center and end of obstacles, height and type ( red- LOOP, green-  RAMP). At the end it will turn green light two times,that mean it accept the obstacles. If youy press and hold player one buttom it will end the seting of obstacles proces.
- If you don't press any buttom the idel animation will begin. 
- The fastest you press the fasstes you run. The white is obstacles which slow down car.
- When the race end it will turn color of the car color
- You can change number of loops in _NUMBER_OF_LOOP define, "main.h"
- Also progress and best time will be display in PC aplication

___
#### DEFINE EXPLAIN:  
#define _DEBUG_MODE		- debug using serial, can caouse memory RAM problem  
#define _DEBUG_GRAV_MAP	- send grav map to pc, catch with RealTerm , .txt-> .csv, plot it with matlab  
#define _TELEMTRY_VIEW 	- use this wth telemtry_viewer (http://www.farrellf.com/TelemetryViewer/) , and open Layout Telemetry_default.txt , 5700 boudrate  
#define _BLUETHOOT		-  future extension, telemtry via bluthoot  
#define _OLED			- not ready future, use SSD1306 0.96 monochrome OLED to dispaly progress  
___
#### HOW TO ADD PLAYER:  
&nbsp;&nbsp;&nbsp;	Edit:  
&nbsp;&nbsp;&nbsp;&nbsp;	_NUMBER_OF_PLAYER   
&nbsp;&nbsp;&nbsp;&nbsp;	Color_player[]  
&nbsp;&nbsp;&nbsp;&nbsp;	Pin_player[]  

___
#### Libraries:  
- led-race : https://github.com/gbarbarov/led-race
- MemoryFree:  https://github.com/maniacbug/MemoryFree
- Adafruit: NeoPixel, GFX, SSD1306 (GFX,SSD1306 until problem with to less RAM this lib not nessesary)
- Adruino : Wire, SPI , SoftwareSerial, HID, EEPROM

___
#### Issue:  
- white and "light" color problem, when you set bright color on all LED, strip of led consume too many current and it turn red/ranbow color
- fix implmentacion of cCar::show_winer  

___
#### TODO:  
- deoxyegen documentation  
- telemtry through WiFi

___
#### Other folder:  
- Matlab : Process data with matalb aplication, eg. plot gravity map (track) https://www.mathworks.com/products/matlab.html
- Visual Code : this is project in   visual studio code , you can copy file to this folderand use this software if you want, this use Arduino. After install plugins  pres F1, type: "Arduino: upload".
- Telemetry: this folder contains visualisation of race. It connect via USB cable to Windows PC . http://www.farrellf.com/TelemetryViewer/

___
#### License:  
GPLv3 